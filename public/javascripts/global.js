// Data structures for the single-page app
var bcxComps = [];
var labels = [];
for (i=0; i<30; i++) { labels.push(i + 1); }
var graphData = {
  labels: labels, // Labels for each line on the graph
  datasets: [],
};

function BcxComp (currencyCode, days) {
  this.currencyCode = currencyCode;
  this.days = days;
  this.avg = null;
  this.vol = null;
}

// Trigger nifty stuff when the DOM loads
$(document).ready( function() {
  console.log("howdy");
  addCurrentRate();
  addComp("USD", 30);
  // addCurrentRate("EUR"); // Not sure how to get this from the API...
  addComp("EUR", 30);
  //console.log(graphData);
});

// Functions for getting, calculating, and adding Bitcoin exchange rates ==============================================

// This can be used to get the current Bitcoin exchange rate (to USD)
function addCurrentRate() {
  console.log("Fetching current rate...")
  var ratePromise = APIHelpers.getCurrentRate();
  ratePromise.done( function(result) {
    console.log("Current rate was retrieved successfully");
    var comp = new BcxComp(result["currency"], 'current');
    comp.avg = result["amount"];
    comp.vol = "NA";
    addCompToTable(comp);
  }).fail( function(error) {
    console.log(error);
  });
}

// This will get the Bitcoin exchange rate for the previous 30 days (hourly data)
function addComp(currencyCode, days) {
  var comp = new BcxComp(currencyCode, days);
  var rates = [];
  switch (days) {
    case 30:
      var bcxPromise = APIHelpers.getBcx30Days(currencyCode); // call the 3rd party API for Bitcoin data
      bcxPromise.done( function(result) {
        console.log("Got Bitcoin exchange rates for " + days + "-day period in " + currencyCode);
        rates = getRatesFromCSV(result); // Parse response body (it should be CSV)
        console.log("first rate is:" + rates[0]);
        //console.log("rates:"+ rates);
        comp = analyzeStats(rates, comp); // Analyze the data and save key data points
        bcxComps.push(comp); // Store the comp data in the global array.
        addCompToTable(comp); // Add the comp to the DOM
        addGraphData(rates, currencyCode, days);
      }).fail( function(error) {
        console.log(error);
      });
    //case 60:
    //case 120:
  }
}

// Takes CSV and converts it into an array of Bitcoin exchange rates
function getRatesFromCSV(csv) {
  console.log("Parsing csv response...");
  var resObjs = $.csv.toObjects(csv);
  var rates = resObjs.map( function (obj) {
    return obj["average"];
  });
  return rates;
}

// Calculate the average and volatility based on a list of exchange rates
// .. and store it in a comp object...
function analyzeStats(rates, comp) {
  var sum = 0;
  var deviations = [];
  var deviationSum = 0;

  // Calculate the average
  for (var i=0; i < rates.length; i++) {
    sum += parseFloat(rates[i]);
  }
  comp.avg = ( sum / rates.length ).toFixed(2);

  // Calculate the standard deviation (aka volatility)
  for (x in rates) {
    var deviationSquared = Math.pow( (rates[x] - comp.avg), 2);
    deviations.push(deviationSquared);
    deviationSum += deviationSquared;
  }
  comp.vol = Math.sqrt( deviationSum / rates.length ).toFixed(2);

  // Return the comp object with the calculated data points
  return comp;
}

function addCompToTable(comp) {
  var text = "";
  var textClass ="";
  if (comp.days == "current") {
    text = "Bitcoin / " + comp.currencyCode + " (current)";
  } else {
    text = "Bitcoin / " + comp.currencyCode + " (" + comp.days + "d)";
  }

  if (comp.currencyCode == "USD") {
    textClass = "cyan";
  } else {
    textClass = "yellow-green";
  }

  $('#stats').append(
    "<tr>" +
      "<td class=\"" + textClass + "\">" +
        text +
      "</td>" +
      "<td>" +
        comp.avg +
      "</td>" +
      "<td>" +
        comp.vol +
      "</td>" +
    "</tr>"
  );
}

// Functions for creating the graph =================================================================================
function addGraphData(rates, currencyCode, days) {
  var ratesToGraph = [];
  var dataset = null;
  switch(days) {
    case 30:
      for (var i=0; i < 30; i++) {
        // since the 30-day data is measured in hours, grab a data point once per day
        // ... aka, once every 24 hours
        ratesToGraph.push(rates[i*24]);
      }
  }
  if (currencyCode == "USD") {
    dataset = {
      label: "USD",
      fillColor: "rgba(0,143,170,0.2)",
      strokeColor: "rgba(0,143,170,1)",
      pointColor: "rgba(0,143,170,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(0,143,170,1)",
      data: ratesToGraph
    }
  } else if (currencyCode == "EUR") {
    dataset = {
      label: "EUR",
      fillColor: "rgba(170,242,0,0.2)",
      strokeColor: "rgba(170,242,0,1)",
      pointColor: "rgba(170,242,0,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(170,242,0,1)",
      data: ratesToGraph
    }

  }
  console.log("Graph data:"+ratesToGraph);
  graphData["datasets"].push(dataset);
  renderGraph();
}

function renderGraph() {
  //var ctx = document.getElementById("graph").getContext("2d");
  var ctx = $("#graph").get(0).getContext("2d");
  var options = {
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%> <%=datasets[i].label%> <%}%></li><%}%></ul>",
  }

  var graph = new Chart(ctx).Line(graphData, options);
}


// API functions =================================================================================================
var APIHelpers = {

  // Gets the current Bitcoin exchange rate to USD
  getCurrentRate: function() {
    var promise = $.Deferred();
    $.ajax({
      type: 'GET',
      url: "https://montanaflynn-bitcoin-exchange-rate.p.mashape.com/prices/spot_rate",
      // Yes this is insecure.  Not important.
      beforeSend: function(xhr){xhr.setRequestHeader("X-Mashape-Key", "U53pyZT5tWmshdINoYV5zLOuAjQap1UdwhyjsnHHa07q6ICQgk");},
      success: function(result){
        promise.resolve(result); 
      },
      error: function() { 
        var error = 'could not fetch bitcoin exchange rate'; 
        promise.reject(error);
      }
    });
    return promise;
  },
  // Gets current foreign exchange rates
  getFx: function() {
    var promise = $.Deferred();
    $.ajax({
      type: 'GET',
      url: "https://montanaflynn-bitcoin-exchange-rate.p.mashape.com/currencies/exchange_rates",
      // Yes this is insecure.  Not important.
      beforeSend: function(xhr){xhr.setRequestHeader("X-Mashape-Key", "U53pyZT5tWmshdINoYV5zLOuAjQap1UdwhyjsnHHa07q6ICQgk");},
      success: function(result){
        promise.resolve(result); 
      },
      error: function() { 
        var error = 'could not fetch foreign exchange rates'; 
        promise.reject(error);
      }
    });
    return promise;
  },
  // Gets Bitcoin exchange rates for the trailing 30 days (to USD, measured hourly)
  getBcx30Days: function(currencyCode) {
    var promise = $.Deferred();
    $.ajax({
      type: 'GET',
      url: "https://community-bitcoinaverage.p.mashape.com/history/" + currencyCode + "/per_hour_monthly_sliding_window.csv",
      // Yes this is insecure.  Not important.
      beforeSend: function(xhr){xhr.setRequestHeader("X-Mashape-Key", "U53pyZT5tWmshdINoYV5zLOuAjQap1UdwhyjsnHHa07q6ICQgk");},
      success: function(result){
        promise.resolve(result); 
      },
      error: function() { 
        var error = 'failed to fetch 30-day bitcoin rates'; 
        promise.reject(error);
      }
    });
    return promise;
  },
  // Gets Bitcoin exchange rates for all of known history (to USD, measured daily)
  getBcxForever: function(currencyCode) {
    var promise = $.Deferred();
    $.ajax({
      type: 'GET',
      url: "https://community-bitcoinaverage.p.mashape.com/history/" + currencyCode + "/per_day_all_time_history.csv",
      // Yes this is insecure. Not important.
      beforeSend: function(xhr){xhr.setRequestHeader("X-Mashape-Key", "U53pyZT5tWmshdINoYV5zLOuAjQap1UdwhyjsnHHa07q6ICQgk");},
      success: function(result){
        promise.resolve(result); 
      },
      error: function() { 
        var error = 'failed to fetch historical bitcoin rates (for all history)'; 
        promise.reject(error);
      }
    });
    return promise;
  }
}